package main

import (
    "os"
    "net/http"
    "io"
)

func serveFile(w http.ResponseWriter, r *http.Request, file string){
    f, err := os.Open(file)
    if err != nil {
        panic(err)
    }
    
    buffer := make([]byte, 16*1024)
    
    for {
        n, err := f.Read(buffer);
        if err != io.EOF && err != nil {
            panic(err)
        }
        if n == 0 {
            break
        }
        
        w.Write(buffer[:n])
    }
}

func main() {
    http.HandleFunc("/manifest", func (w http.ResponseWriter, r *http.Request){
        serveFile(w, r, "segz/test.txt")
    })
    
    http.HandleFunc("/player", func (w http.ResponseWriter, r *http.Request){
        serveFile(w, r, "SV.html")
    })
    
    http.HandleFunc("/segments", func(w http.ResponseWriter, r *http.Request){
        var handle = http.StripPrefix("/segments", http.FileServer(http.Dir("segz/")))
        handle.ServeHTTP(w, r)
    })
    
    http.ListenAndServe(":8080", nil);
}