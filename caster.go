package main

import (
    "./lib/shout"
    "os"
    "io"
    "log"
)

func main() {
    source := shout.Shout{
        Host: "rosetta.shoutca.st",
    	Port: 8275,
    	User: "mccolljr",
    	Password: "mofradio",
    	Format: shout.FORMAT_MP3,
    	Mount: "stream",
    	Protocol: shout.PROTOCOL_HTTP,
    }
    
    stream, err := source.Open()
    if err != nil {
        log.Fatal("Error connecting:", err)
    }
    
    defer source.Close();
    
    file, err := os.Open("x.mp3");
    if err != nil {
        log.Fatal("Error loading song:", err)
    }
    
    buffer := make([]byte, 16*1024)
    
    for {
        n, err := file.Read(buffer)
        if err != nil && err != io.EOF { panic(err) }
        //log.Printf("%v bytes read", n)
        if n == 0 { break }

        stream <- buffer[:n]
    }
}