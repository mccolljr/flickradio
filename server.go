package main

import (
    "net/http"
    "strconv"
    "os"
    "os/exec"
    "io"
    "fmt"
    "log"
    "./lib/render"
    "./lib/dbi"
)

func processSongUpload(r *http.Request) error {
    // first, get the song file
    file, f2, err := r.FormFile("song_file")
    defer file.Close()
    if err != nil {
        return err
    }
    
    log.Println(f2.Filename)
    
    song := dbi.NewSong()
    
    song.Name = r.FormValue("song_title")
    // TODO
    // ArtistId should be determined by the logged in user
    song.ArtistId = 1
    song.GenreId, err= strconv.Atoi(r.FormValue("song_genre"))
    if err != nil {
        return err
    }
    
    tempFile := fmt.Sprintf(".temp/%s", song.SID)
    defer os.Remove(tempFile)
    temp, err := os.Create(tempFile)
    if err != nil {
        return err
    }
    
    _, err = io.Copy(temp, file)
    if err != nil {
        return err
    }
    
    songPath := fmt.Sprintf("media/songs/%s", song.SID)
    err = os.Mkdir(songPath, 0777)
    if err != nil {
        return err
    }
    
    convFile := fmt.Sprintf("%s/media.mp4", songPath)
    conv := exec.Command("ffmpeg", "-i", tempFile, "-f",  "mp4",  "-vn", convFile)
    
    info, err := conv.CombinedOutput()
    if err != nil {
        defer os.RemoveAll(songPath)
        log.Printf("Conversion Error: %s", info)
        return err
    }
    
    splitFile := fmt.Sprintf("%s/play.mpd", songPath)
    split := exec.Command("mp4box", "-dash", "1000", convFile, "-out", splitFile, "-profile", "live")
    
    info, err = split.CombinedOutput()
    if err != nil {
        defer os.RemoveAll(songPath)
        log.Printf("Segmenting Error: %s\n", info)
        return err
    }
    
    err = song.Save()
    if err != nil {
        defer os.RemoveAll(songPath)
        return err
    }
    
    // FIXME
    // the refactoring will be fun. I'm sure.
    
    // TODO
    // a lot... but now I can start managing the collection
    // and serving files from the songs folder to DASH players
    // and hopefully that means... magic.
    // or something. I dont know.
    // oh well.
    
    return nil
    
}

func handleUpload(w http.ResponseWriter, r *http.Request){
    var failure error
    
    switch r.Method {
    case "GET":
        var gnrz dbi.Genres
        gnrz, failure = dbi.GetGenres()
        if failure != nil {
            goto FAILURE
        }
        ctx := struct {
            Artist string
            Genres dbi.Genres
        }{"Aardvarkz", gnrz}
        failure = render.Page(w, "views/upload.html", ctx)
        if failure != nil {
            goto FAILURE
        }
    case "POST":
        failure = processSongUpload(r)
        if failure != nil {
            goto FAILURE
        }
    default:
        render.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
    }     
    
    return
    
    // I used a goto...
    // will the world fail to a catastrophic and horrifying end...?
FAILURE:
    render.Error(w, "Something went oh so wrong!", 500)
    log.Println("Upload:", failure);
}

func servePlayer(w http.ResponseWriter, r *http.Request){
    if r.Method != "GET" {
        render.Error(w, "Bad Request", http.StatusBadRequest)
        return
    }
    
    songs, err := dbi.GetSongs()
    if err != nil {
        log.Printf("Error loading songs for player: %s", err)
        render.Error(w, "Something's gone so... soo... wrong", http.StatusInternalServerError)
        return
    }
    
    ctx := struct{
        Songs []*dbi.Song
    }{songs}
    
    if err = render.Page(w, "views/player.html", ctx); err != nil {
        log.Printf("Error serving player: %s", err)
        render.Error(w, "Something's so wrong...",  http.StatusInternalServerError)
        return
    }
}

func main(){
    stack := http.NewServeMux()
    
    stack.HandleFunc("/player", servePlayer)
    stack.HandleFunc("/upload", handleUpload)
    stack.HandleFunc("/media/songs/", func(w http.ResponseWriter, r *http.Request){
        w.Header().Set("Access-Control-Allow-Origin", "*")
        w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS")
        w.Header().Set("Access-Control-Allow-Headers", "*")
        if r.Method == "OPTIONS" {
            w.WriteHeader(200)
            return
        }
        log.Println("serving", r.URL.Path)
        http.ServeFile(w, r, string([]byte(r.URL.String())[1:]))
    })
    log.Fatal(http.ListenAndServe(fmt.Sprintf(":%s", os.Getenv("PORT")), stack))
}