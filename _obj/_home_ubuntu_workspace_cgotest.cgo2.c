#line 26 "/home/ubuntu/workspace/cgotest.go"


#include <stdlib.h>
#include <shout/shout.h>



// Usual nonsense: if x and y are not equal, the type will be invalid
// (have a negative array count) and an inscrutable error will come
// out of the compiler and hopefully mention "name".
#define __cgo_compile_assert_eq(x, y, name) typedef char name[(x-y)*(x-y)*-2+1];

// Check at compile time that the sizes we use match our expectations.
#define __cgo_size_assert(t, n) __cgo_compile_assert_eq(sizeof(t), n, _cgo_sizeof_##t##_is_not_##n)

__cgo_size_assert(char, 1)
__cgo_size_assert(short, 2)
__cgo_size_assert(int, 4)
typedef long long __cgo_long_long;
__cgo_size_assert(__cgo_long_long, 8)
__cgo_size_assert(float, 4)
__cgo_size_assert(double, 8)

extern char* _cgo_topofstack(void);

#include <errno.h>
#include <string.h>

void
_cgo_610e07f9925a_Cfunc_free(void *v)
{
	struct {
		void* p0;
	} __attribute__((__packed__, __gcc_struct__)) *a = v;
	free((void*)a->p0);
}

void
_cgo_610e07f9925a_Cfunc_shout_close(void *v)
{
	struct {
		shout_t* p0;
		int r;
		char __pad12[4];
	} __attribute__((__packed__, __gcc_struct__)) *a = v;
	char *stktop = _cgo_topofstack();
	__typeof__(a->r) r = shout_close((void*)a->p0);
	a = (void*)((char*)a + (_cgo_topofstack() - stktop));
	a->r = r;
}

void
_cgo_610e07f9925a_Cfunc_shout_free(void *v)
{
	struct {
		shout_t* p0;
	} __attribute__((__packed__, __gcc_struct__)) *a = v;
	shout_free((void*)a->p0);
}

void
_cgo_610e07f9925a_Cfunc_shout_get_errno(void *v)
{
	struct {
		shout_t* p0;
		int r;
		char __pad12[4];
	} __attribute__((__packed__, __gcc_struct__)) *a = v;
	char *stktop = _cgo_topofstack();
	__typeof__(a->r) r = shout_get_errno((void*)a->p0);
	a = (void*)((char*)a + (_cgo_topofstack() - stktop));
	a->r = r;
}

void
_cgo_610e07f9925a_Cfunc_shout_get_error(void *v)
{
	struct {
		shout_t* p0;
		const char* r;
	} __attribute__((__packed__, __gcc_struct__)) *a = v;
	char *stktop = _cgo_topofstack();
	__typeof__(a->r) r = (__typeof__(a->r)) shout_get_error((void*)a->p0);
	a = (void*)((char*)a + (_cgo_topofstack() - stktop));
	a->r = r;
}

void
_cgo_610e07f9925a_Cfunc_shout_init(void *v)
{
	struct {
		char unused;
	} __attribute__((__packed__, __gcc_struct__)) *a = v;
	shout_init();
}

void
_cgo_610e07f9925a_Cfunc_shout_metadata_add(void *v)
{
	struct {
		shout_metadata_t* p0;
		char* p1;
		char* p2;
		int r;
		char __pad28[4];
	} __attribute__((__packed__, __gcc_struct__)) *a = v;
	char *stktop = _cgo_topofstack();
	__typeof__(a->r) r = shout_metadata_add((void*)a->p0, (void*)a->p1, (void*)a->p2);
	a = (void*)((char*)a + (_cgo_topofstack() - stktop));
	a->r = r;
}

void
_cgo_610e07f9925a_Cfunc_shout_metadata_free(void *v)
{
	struct {
		shout_metadata_t* p0;
	} __attribute__((__packed__, __gcc_struct__)) *a = v;
	shout_metadata_free((void*)a->p0);
}

void
_cgo_610e07f9925a_Cfunc_shout_metadata_new(void *v)
{
	struct {
		const shout_metadata_t* r;
	} __attribute__((__packed__, __gcc_struct__)) *a = v;
	char *stktop = _cgo_topofstack();
	__typeof__(a->r) r = (__typeof__(a->r)) shout_metadata_new();
	a = (void*)((char*)a + (_cgo_topofstack() - stktop));
	a->r = r;
}

void
_cgo_610e07f9925a_Cfunc_shout_new(void *v)
{
	struct {
		const shout_t* r;
	} __attribute__((__packed__, __gcc_struct__)) *a = v;
	char *stktop = _cgo_topofstack();
	__typeof__(a->r) r = (__typeof__(a->r)) shout_new();
	a = (void*)((char*)a + (_cgo_topofstack() - stktop));
	a->r = r;
}

void
_cgo_610e07f9925a_Cfunc_shout_open(void *v)
{
	struct {
		shout_t* p0;
		int r;
		char __pad12[4];
	} __attribute__((__packed__, __gcc_struct__)) *a = v;
	char *stktop = _cgo_topofstack();
	__typeof__(a->r) r = shout_open((void*)a->p0);
	a = (void*)((char*)a + (_cgo_topofstack() - stktop));
	a->r = r;
}

void
_cgo_610e07f9925a_Cfunc_shout_send(void *v)
{
	struct {
		shout_t* p0;
		unsigned char* p1;
		size_t p2;
		int r;
		char __pad28[4];
	} __attribute__((__packed__, __gcc_struct__)) *a = v;
	char *stktop = _cgo_topofstack();
	__typeof__(a->r) r = shout_send((void*)a->p0, (void*)a->p1, a->p2);
	a = (void*)((char*)a + (_cgo_topofstack() - stktop));
	a->r = r;
}

void
_cgo_610e07f9925a_Cfunc_shout_set_format(void *v)
{
	struct {
		shout_t* p0;
		unsigned int p1;
		char __pad12[4];
		int r;
		char __pad20[4];
	} __attribute__((__packed__, __gcc_struct__)) *a = v;
	char *stktop = _cgo_topofstack();
	__typeof__(a->r) r = shout_set_format((void*)a->p0, a->p1);
	a = (void*)((char*)a + (_cgo_topofstack() - stktop));
	a->r = r;
}

void
_cgo_610e07f9925a_Cfunc_shout_set_host(void *v)
{
	struct {
		shout_t* p0;
		char* p1;
		int r;
		char __pad20[4];
	} __attribute__((__packed__, __gcc_struct__)) *a = v;
	char *stktop = _cgo_topofstack();
	__typeof__(a->r) r = shout_set_host((void*)a->p0, (void*)a->p1);
	a = (void*)((char*)a + (_cgo_topofstack() - stktop));
	a->r = r;
}

void
_cgo_610e07f9925a_Cfunc_shout_set_metadata(void *v)
{
	struct {
		shout_t* p0;
		shout_metadata_t* p1;
		int r;
		char __pad20[4];
	} __attribute__((__packed__, __gcc_struct__)) *a = v;
	char *stktop = _cgo_topofstack();
	__typeof__(a->r) r = shout_set_metadata((void*)a->p0, (void*)a->p1);
	a = (void*)((char*)a + (_cgo_topofstack() - stktop));
	a->r = r;
}

void
_cgo_610e07f9925a_Cfunc_shout_set_mount(void *v)
{
	struct {
		shout_t* p0;
		char* p1;
		int r;
		char __pad20[4];
	} __attribute__((__packed__, __gcc_struct__)) *a = v;
	char *stktop = _cgo_topofstack();
	__typeof__(a->r) r = shout_set_mount((void*)a->p0, (void*)a->p1);
	a = (void*)((char*)a + (_cgo_topofstack() - stktop));
	a->r = r;
}

void
_cgo_610e07f9925a_Cfunc_shout_set_password(void *v)
{
	struct {
		shout_t* p0;
		char* p1;
		int r;
		char __pad20[4];
	} __attribute__((__packed__, __gcc_struct__)) *a = v;
	char *stktop = _cgo_topofstack();
	__typeof__(a->r) r = shout_set_password((void*)a->p0, (void*)a->p1);
	a = (void*)((char*)a + (_cgo_topofstack() - stktop));
	a->r = r;
}

void
_cgo_610e07f9925a_Cfunc_shout_set_port(void *v)
{
	struct {
		shout_t* p0;
		short unsigned int p1;
		char __pad10[6];
		int r;
		char __pad20[4];
	} __attribute__((__packed__, __gcc_struct__)) *a = v;
	char *stktop = _cgo_topofstack();
	__typeof__(a->r) r = shout_set_port((void*)a->p0, a->p1);
	a = (void*)((char*)a + (_cgo_topofstack() - stktop));
	a->r = r;
}

void
_cgo_610e07f9925a_Cfunc_shout_set_protocol(void *v)
{
	struct {
		shout_t* p0;
		unsigned int p1;
		char __pad12[4];
		int r;
		char __pad20[4];
	} __attribute__((__packed__, __gcc_struct__)) *a = v;
	char *stktop = _cgo_topofstack();
	__typeof__(a->r) r = shout_set_protocol((void*)a->p0, a->p1);
	a = (void*)((char*)a + (_cgo_topofstack() - stktop));
	a->r = r;
}

void
_cgo_610e07f9925a_Cfunc_shout_set_user(void *v)
{
	struct {
		shout_t* p0;
		char* p1;
		int r;
		char __pad20[4];
	} __attribute__((__packed__, __gcc_struct__)) *a = v;
	char *stktop = _cgo_topofstack();
	__typeof__(a->r) r = shout_set_user((void*)a->p0, (void*)a->p1);
	a = (void*)((char*)a + (_cgo_topofstack() - stktop));
	a->r = r;
}

void
_cgo_610e07f9925a_Cfunc_shout_shutdown(void *v)
{
	struct {
		char unused;
	} __attribute__((__packed__, __gcc_struct__)) *a = v;
	shout_shutdown();
}

void
_cgo_610e07f9925a_Cfunc_shout_sync(void *v)
{
	struct {
		shout_t* p0;
	} __attribute__((__packed__, __gcc_struct__)) *a = v;
	shout_sync((void*)a->p0);
}

