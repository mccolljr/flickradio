// Created by cgo - DO NOT EDIT

package main

import "unsafe"

import _ "runtime/cgo"

import "syscall"

var _ syscall.Errno
func _Cgo_ptr(ptr unsafe.Pointer) unsafe.Pointer { return ptr }

//go:linkname _Cgo_always_false runtime.cgoAlwaysFalse
var _Cgo_always_false bool
//go:linkname _Cgo_use runtime.cgoUse
func _Cgo_use(interface{})
type _Ctype_char int8

type _Ctype_int int32

type _Ctype_shout_metadata_t _Ctype_struct__util_dict

type _Ctype_shout_t _Ctype_struct_shout

type _Ctype_size_t _Ctype_ulong

type _Ctype_struct__util_dict struct{}

type _Ctype_struct_shout struct{}

type _Ctype_struct_shout_metadata_t struct{}

type _Ctype_uint uint32

type _Ctype_ulong uint64

type _Ctype_unsignedchar uint8

type _Ctype_ushort uint16

type _Ctype_void [0]byte

//go:linkname _cgo_runtime_cgocall runtime.cgocall
func _cgo_runtime_cgocall(unsafe.Pointer, uintptr) int32

//go:linkname _cgo_runtime_cmalloc runtime.cmalloc
func _cgo_runtime_cmalloc(uintptr) unsafe.Pointer

//go:linkname _cgo_runtime_cgocallback runtime.cgocallback
func _cgo_runtime_cgocallback(unsafe.Pointer, unsafe.Pointer, uintptr)
const _Cconst_SHOUTERR_SUCCESS = 0x0


func _Cfunc_CString(s string) *_Ctype_char {
	p := _cgo_runtime_cmalloc(uintptr(len(s)+1))
	pp := (*[1<<30]byte)(p)
	copy(pp[:], s)
	pp[len(s)] = 0
	return (*_Ctype_char)(p)
}

//go:linkname _cgo_runtime_gostring runtime.gostring
func _cgo_runtime_gostring(*_Ctype_char) string

func _Cfunc_GoString(p *_Ctype_char) string {
	return _cgo_runtime_gostring(p)
}
//go:cgo_import_static _cgo_610e07f9925a_Cfunc_free
//go:linkname __cgofn__cgo_610e07f9925a_Cfunc_free _cgo_610e07f9925a_Cfunc_free
var __cgofn__cgo_610e07f9925a_Cfunc_free byte
var _cgo_610e07f9925a_Cfunc_free = unsafe.Pointer(&__cgofn__cgo_610e07f9925a_Cfunc_free)

func _Cfunc_free(p0 unsafe.Pointer) (r1 _Ctype_void) {
	_cgo_runtime_cgocall(_cgo_610e07f9925a_Cfunc_free, uintptr(unsafe.Pointer(&p0)))
	if _Cgo_always_false {
		_Cgo_use(p0)
	}
	return
}
//go:cgo_import_static _cgo_610e07f9925a_Cfunc_shout_close
//go:linkname __cgofn__cgo_610e07f9925a_Cfunc_shout_close _cgo_610e07f9925a_Cfunc_shout_close
var __cgofn__cgo_610e07f9925a_Cfunc_shout_close byte
var _cgo_610e07f9925a_Cfunc_shout_close = unsafe.Pointer(&__cgofn__cgo_610e07f9925a_Cfunc_shout_close)

func _Cfunc_shout_close(p0 *_Ctype_struct_shout) (r1 _Ctype_int) {
	_cgo_runtime_cgocall(_cgo_610e07f9925a_Cfunc_shout_close, uintptr(unsafe.Pointer(&p0)))
	if _Cgo_always_false {
		_Cgo_use(p0)
	}
	return
}
//go:cgo_import_static _cgo_610e07f9925a_Cfunc_shout_free
//go:linkname __cgofn__cgo_610e07f9925a_Cfunc_shout_free _cgo_610e07f9925a_Cfunc_shout_free
var __cgofn__cgo_610e07f9925a_Cfunc_shout_free byte
var _cgo_610e07f9925a_Cfunc_shout_free = unsafe.Pointer(&__cgofn__cgo_610e07f9925a_Cfunc_shout_free)

func _Cfunc_shout_free(p0 *_Ctype_struct_shout) (r1 _Ctype_void) {
	_cgo_runtime_cgocall(_cgo_610e07f9925a_Cfunc_shout_free, uintptr(unsafe.Pointer(&p0)))
	if _Cgo_always_false {
		_Cgo_use(p0)
	}
	return
}
//go:cgo_import_static _cgo_610e07f9925a_Cfunc_shout_get_errno
//go:linkname __cgofn__cgo_610e07f9925a_Cfunc_shout_get_errno _cgo_610e07f9925a_Cfunc_shout_get_errno
var __cgofn__cgo_610e07f9925a_Cfunc_shout_get_errno byte
var _cgo_610e07f9925a_Cfunc_shout_get_errno = unsafe.Pointer(&__cgofn__cgo_610e07f9925a_Cfunc_shout_get_errno)

func _Cfunc_shout_get_errno(p0 *_Ctype_struct_shout) (r1 _Ctype_int) {
	_cgo_runtime_cgocall(_cgo_610e07f9925a_Cfunc_shout_get_errno, uintptr(unsafe.Pointer(&p0)))
	if _Cgo_always_false {
		_Cgo_use(p0)
	}
	return
}
//go:cgo_import_static _cgo_610e07f9925a_Cfunc_shout_get_error
//go:linkname __cgofn__cgo_610e07f9925a_Cfunc_shout_get_error _cgo_610e07f9925a_Cfunc_shout_get_error
var __cgofn__cgo_610e07f9925a_Cfunc_shout_get_error byte
var _cgo_610e07f9925a_Cfunc_shout_get_error = unsafe.Pointer(&__cgofn__cgo_610e07f9925a_Cfunc_shout_get_error)

func _Cfunc_shout_get_error(p0 *_Ctype_struct_shout) (r1 *_Ctype_char) {
	_cgo_runtime_cgocall(_cgo_610e07f9925a_Cfunc_shout_get_error, uintptr(unsafe.Pointer(&p0)))
	if _Cgo_always_false {
		_Cgo_use(p0)
	}
	return
}
//go:cgo_import_static _cgo_610e07f9925a_Cfunc_shout_init
//go:linkname __cgofn__cgo_610e07f9925a_Cfunc_shout_init _cgo_610e07f9925a_Cfunc_shout_init
var __cgofn__cgo_610e07f9925a_Cfunc_shout_init byte
var _cgo_610e07f9925a_Cfunc_shout_init = unsafe.Pointer(&__cgofn__cgo_610e07f9925a_Cfunc_shout_init)

func _Cfunc_shout_init() (r1 _Ctype_void) {
	_cgo_runtime_cgocall(_cgo_610e07f9925a_Cfunc_shout_init, uintptr(unsafe.Pointer(&r1)))
	if _Cgo_always_false {
	}
	return
}
//go:cgo_import_static _cgo_610e07f9925a_Cfunc_shout_metadata_add
//go:linkname __cgofn__cgo_610e07f9925a_Cfunc_shout_metadata_add _cgo_610e07f9925a_Cfunc_shout_metadata_add
var __cgofn__cgo_610e07f9925a_Cfunc_shout_metadata_add byte
var _cgo_610e07f9925a_Cfunc_shout_metadata_add = unsafe.Pointer(&__cgofn__cgo_610e07f9925a_Cfunc_shout_metadata_add)

func _Cfunc_shout_metadata_add(p0 *_Ctype_struct__util_dict, p1 *_Ctype_char, p2 *_Ctype_char) (r1 _Ctype_int) {
	_cgo_runtime_cgocall(_cgo_610e07f9925a_Cfunc_shout_metadata_add, uintptr(unsafe.Pointer(&p0)))
	if _Cgo_always_false {
		_Cgo_use(p0)
		_Cgo_use(p1)
		_Cgo_use(p2)
	}
	return
}
//go:cgo_import_static _cgo_610e07f9925a_Cfunc_shout_metadata_free
//go:linkname __cgofn__cgo_610e07f9925a_Cfunc_shout_metadata_free _cgo_610e07f9925a_Cfunc_shout_metadata_free
var __cgofn__cgo_610e07f9925a_Cfunc_shout_metadata_free byte
var _cgo_610e07f9925a_Cfunc_shout_metadata_free = unsafe.Pointer(&__cgofn__cgo_610e07f9925a_Cfunc_shout_metadata_free)

func _Cfunc_shout_metadata_free(p0 *_Ctype_struct__util_dict) (r1 _Ctype_void) {
	_cgo_runtime_cgocall(_cgo_610e07f9925a_Cfunc_shout_metadata_free, uintptr(unsafe.Pointer(&p0)))
	if _Cgo_always_false {
		_Cgo_use(p0)
	}
	return
}
//go:cgo_import_static _cgo_610e07f9925a_Cfunc_shout_metadata_new
//go:linkname __cgofn__cgo_610e07f9925a_Cfunc_shout_metadata_new _cgo_610e07f9925a_Cfunc_shout_metadata_new
var __cgofn__cgo_610e07f9925a_Cfunc_shout_metadata_new byte
var _cgo_610e07f9925a_Cfunc_shout_metadata_new = unsafe.Pointer(&__cgofn__cgo_610e07f9925a_Cfunc_shout_metadata_new)

func _Cfunc_shout_metadata_new() (r1 *_Ctype_struct__util_dict) {
	_cgo_runtime_cgocall(_cgo_610e07f9925a_Cfunc_shout_metadata_new, uintptr(unsafe.Pointer(&r1)))
	if _Cgo_always_false {
	}
	return
}
//go:cgo_import_static _cgo_610e07f9925a_Cfunc_shout_new
//go:linkname __cgofn__cgo_610e07f9925a_Cfunc_shout_new _cgo_610e07f9925a_Cfunc_shout_new
var __cgofn__cgo_610e07f9925a_Cfunc_shout_new byte
var _cgo_610e07f9925a_Cfunc_shout_new = unsafe.Pointer(&__cgofn__cgo_610e07f9925a_Cfunc_shout_new)

func _Cfunc_shout_new() (r1 *_Ctype_struct_shout) {
	_cgo_runtime_cgocall(_cgo_610e07f9925a_Cfunc_shout_new, uintptr(unsafe.Pointer(&r1)))
	if _Cgo_always_false {
	}
	return
}
//go:cgo_import_static _cgo_610e07f9925a_Cfunc_shout_open
//go:linkname __cgofn__cgo_610e07f9925a_Cfunc_shout_open _cgo_610e07f9925a_Cfunc_shout_open
var __cgofn__cgo_610e07f9925a_Cfunc_shout_open byte
var _cgo_610e07f9925a_Cfunc_shout_open = unsafe.Pointer(&__cgofn__cgo_610e07f9925a_Cfunc_shout_open)

func _Cfunc_shout_open(p0 *_Ctype_struct_shout) (r1 _Ctype_int) {
	_cgo_runtime_cgocall(_cgo_610e07f9925a_Cfunc_shout_open, uintptr(unsafe.Pointer(&p0)))
	if _Cgo_always_false {
		_Cgo_use(p0)
	}
	return
}
//go:cgo_import_static _cgo_610e07f9925a_Cfunc_shout_send
//go:linkname __cgofn__cgo_610e07f9925a_Cfunc_shout_send _cgo_610e07f9925a_Cfunc_shout_send
var __cgofn__cgo_610e07f9925a_Cfunc_shout_send byte
var _cgo_610e07f9925a_Cfunc_shout_send = unsafe.Pointer(&__cgofn__cgo_610e07f9925a_Cfunc_shout_send)

func _Cfunc_shout_send(p0 *_Ctype_struct_shout, p1 *_Ctype_unsignedchar, p2 _Ctype_size_t) (r1 _Ctype_int) {
	_cgo_runtime_cgocall(_cgo_610e07f9925a_Cfunc_shout_send, uintptr(unsafe.Pointer(&p0)))
	if _Cgo_always_false {
		_Cgo_use(p0)
		_Cgo_use(p1)
		_Cgo_use(p2)
	}
	return
}
//go:cgo_import_static _cgo_610e07f9925a_Cfunc_shout_set_format
//go:linkname __cgofn__cgo_610e07f9925a_Cfunc_shout_set_format _cgo_610e07f9925a_Cfunc_shout_set_format
var __cgofn__cgo_610e07f9925a_Cfunc_shout_set_format byte
var _cgo_610e07f9925a_Cfunc_shout_set_format = unsafe.Pointer(&__cgofn__cgo_610e07f9925a_Cfunc_shout_set_format)

func _Cfunc_shout_set_format(p0 *_Ctype_struct_shout, p1 _Ctype_uint) (r1 _Ctype_int) {
	_cgo_runtime_cgocall(_cgo_610e07f9925a_Cfunc_shout_set_format, uintptr(unsafe.Pointer(&p0)))
	if _Cgo_always_false {
		_Cgo_use(p0)
		_Cgo_use(p1)
	}
	return
}
//go:cgo_import_static _cgo_610e07f9925a_Cfunc_shout_set_host
//go:linkname __cgofn__cgo_610e07f9925a_Cfunc_shout_set_host _cgo_610e07f9925a_Cfunc_shout_set_host
var __cgofn__cgo_610e07f9925a_Cfunc_shout_set_host byte
var _cgo_610e07f9925a_Cfunc_shout_set_host = unsafe.Pointer(&__cgofn__cgo_610e07f9925a_Cfunc_shout_set_host)

func _Cfunc_shout_set_host(p0 *_Ctype_struct_shout, p1 *_Ctype_char) (r1 _Ctype_int) {
	_cgo_runtime_cgocall(_cgo_610e07f9925a_Cfunc_shout_set_host, uintptr(unsafe.Pointer(&p0)))
	if _Cgo_always_false {
		_Cgo_use(p0)
		_Cgo_use(p1)
	}
	return
}
//go:cgo_import_static _cgo_610e07f9925a_Cfunc_shout_set_metadata
//go:linkname __cgofn__cgo_610e07f9925a_Cfunc_shout_set_metadata _cgo_610e07f9925a_Cfunc_shout_set_metadata
var __cgofn__cgo_610e07f9925a_Cfunc_shout_set_metadata byte
var _cgo_610e07f9925a_Cfunc_shout_set_metadata = unsafe.Pointer(&__cgofn__cgo_610e07f9925a_Cfunc_shout_set_metadata)

func _Cfunc_shout_set_metadata(p0 *_Ctype_struct_shout, p1 *_Ctype_struct__util_dict) (r1 _Ctype_int) {
	_cgo_runtime_cgocall(_cgo_610e07f9925a_Cfunc_shout_set_metadata, uintptr(unsafe.Pointer(&p0)))
	if _Cgo_always_false {
		_Cgo_use(p0)
		_Cgo_use(p1)
	}
	return
}
//go:cgo_import_static _cgo_610e07f9925a_Cfunc_shout_set_mount
//go:linkname __cgofn__cgo_610e07f9925a_Cfunc_shout_set_mount _cgo_610e07f9925a_Cfunc_shout_set_mount
var __cgofn__cgo_610e07f9925a_Cfunc_shout_set_mount byte
var _cgo_610e07f9925a_Cfunc_shout_set_mount = unsafe.Pointer(&__cgofn__cgo_610e07f9925a_Cfunc_shout_set_mount)

func _Cfunc_shout_set_mount(p0 *_Ctype_struct_shout, p1 *_Ctype_char) (r1 _Ctype_int) {
	_cgo_runtime_cgocall(_cgo_610e07f9925a_Cfunc_shout_set_mount, uintptr(unsafe.Pointer(&p0)))
	if _Cgo_always_false {
		_Cgo_use(p0)
		_Cgo_use(p1)
	}
	return
}
//go:cgo_import_static _cgo_610e07f9925a_Cfunc_shout_set_password
//go:linkname __cgofn__cgo_610e07f9925a_Cfunc_shout_set_password _cgo_610e07f9925a_Cfunc_shout_set_password
var __cgofn__cgo_610e07f9925a_Cfunc_shout_set_password byte
var _cgo_610e07f9925a_Cfunc_shout_set_password = unsafe.Pointer(&__cgofn__cgo_610e07f9925a_Cfunc_shout_set_password)

func _Cfunc_shout_set_password(p0 *_Ctype_struct_shout, p1 *_Ctype_char) (r1 _Ctype_int) {
	_cgo_runtime_cgocall(_cgo_610e07f9925a_Cfunc_shout_set_password, uintptr(unsafe.Pointer(&p0)))
	if _Cgo_always_false {
		_Cgo_use(p0)
		_Cgo_use(p1)
	}
	return
}
//go:cgo_import_static _cgo_610e07f9925a_Cfunc_shout_set_port
//go:linkname __cgofn__cgo_610e07f9925a_Cfunc_shout_set_port _cgo_610e07f9925a_Cfunc_shout_set_port
var __cgofn__cgo_610e07f9925a_Cfunc_shout_set_port byte
var _cgo_610e07f9925a_Cfunc_shout_set_port = unsafe.Pointer(&__cgofn__cgo_610e07f9925a_Cfunc_shout_set_port)

func _Cfunc_shout_set_port(p0 *_Ctype_struct_shout, p1 _Ctype_ushort) (r1 _Ctype_int) {
	_cgo_runtime_cgocall(_cgo_610e07f9925a_Cfunc_shout_set_port, uintptr(unsafe.Pointer(&p0)))
	if _Cgo_always_false {
		_Cgo_use(p0)
		_Cgo_use(p1)
	}
	return
}
//go:cgo_import_static _cgo_610e07f9925a_Cfunc_shout_set_protocol
//go:linkname __cgofn__cgo_610e07f9925a_Cfunc_shout_set_protocol _cgo_610e07f9925a_Cfunc_shout_set_protocol
var __cgofn__cgo_610e07f9925a_Cfunc_shout_set_protocol byte
var _cgo_610e07f9925a_Cfunc_shout_set_protocol = unsafe.Pointer(&__cgofn__cgo_610e07f9925a_Cfunc_shout_set_protocol)

func _Cfunc_shout_set_protocol(p0 *_Ctype_struct_shout, p1 _Ctype_uint) (r1 _Ctype_int) {
	_cgo_runtime_cgocall(_cgo_610e07f9925a_Cfunc_shout_set_protocol, uintptr(unsafe.Pointer(&p0)))
	if _Cgo_always_false {
		_Cgo_use(p0)
		_Cgo_use(p1)
	}
	return
}
//go:cgo_import_static _cgo_610e07f9925a_Cfunc_shout_set_user
//go:linkname __cgofn__cgo_610e07f9925a_Cfunc_shout_set_user _cgo_610e07f9925a_Cfunc_shout_set_user
var __cgofn__cgo_610e07f9925a_Cfunc_shout_set_user byte
var _cgo_610e07f9925a_Cfunc_shout_set_user = unsafe.Pointer(&__cgofn__cgo_610e07f9925a_Cfunc_shout_set_user)

func _Cfunc_shout_set_user(p0 *_Ctype_struct_shout, p1 *_Ctype_char) (r1 _Ctype_int) {
	_cgo_runtime_cgocall(_cgo_610e07f9925a_Cfunc_shout_set_user, uintptr(unsafe.Pointer(&p0)))
	if _Cgo_always_false {
		_Cgo_use(p0)
		_Cgo_use(p1)
	}
	return
}
//go:cgo_import_static _cgo_610e07f9925a_Cfunc_shout_shutdown
//go:linkname __cgofn__cgo_610e07f9925a_Cfunc_shout_shutdown _cgo_610e07f9925a_Cfunc_shout_shutdown
var __cgofn__cgo_610e07f9925a_Cfunc_shout_shutdown byte
var _cgo_610e07f9925a_Cfunc_shout_shutdown = unsafe.Pointer(&__cgofn__cgo_610e07f9925a_Cfunc_shout_shutdown)

func _Cfunc_shout_shutdown() (r1 _Ctype_void) {
	_cgo_runtime_cgocall(_cgo_610e07f9925a_Cfunc_shout_shutdown, uintptr(unsafe.Pointer(&r1)))
	if _Cgo_always_false {
	}
	return
}
//go:cgo_import_static _cgo_610e07f9925a_Cfunc_shout_sync
//go:linkname __cgofn__cgo_610e07f9925a_Cfunc_shout_sync _cgo_610e07f9925a_Cfunc_shout_sync
var __cgofn__cgo_610e07f9925a_Cfunc_shout_sync byte
var _cgo_610e07f9925a_Cfunc_shout_sync = unsafe.Pointer(&__cgofn__cgo_610e07f9925a_Cfunc_shout_sync)

func _Cfunc_shout_sync(p0 *_Ctype_struct_shout) (r1 _Ctype_void) {
	_cgo_runtime_cgocall(_cgo_610e07f9925a_Cfunc_shout_sync, uintptr(unsafe.Pointer(&p0)))
	if _Cgo_always_false {
		_Cgo_use(p0)
	}
	return
}
