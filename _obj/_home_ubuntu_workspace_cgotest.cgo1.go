// Created by cgo - DO NOT EDIT

//line /home/ubuntu/workspace/cgotest.go:19
package main
//line /home/ubuntu/workspace/cgotest.go:22

//line /home/ubuntu/workspace/cgotest.go:21
import (
	"fmt"
	"unsafe"
)
//line /home/ubuntu/workspace/cgotest.go:34

//line /home/ubuntu/workspace/cgotest.go:33
const (
	BUFFER_SIZE = 8192
)
//line /home/ubuntu/workspace/cgotest.go:38

//line /home/ubuntu/workspace/cgotest.go:37
const (
//line /home/ubuntu/workspace/cgotest.go:39
	SHOUTERR_SUCCESS	= 0
	SHOUTERR_INSANE		= -1
	SHOUTERR_NOCORRECT	= -2
	SHOUTERR_NOLOGIN	= -3
	SHOUTERR_SOCKET		= -4
	SHOUTERR_MALLOC		= -5
	SHOUTERR_METADATA	= -6
	SHOUTERR_CONNECTED	= -7
	SHOUTERR_UNCONNECTED	= -8
	SHOUTERR_UNSUPPORTED	= -9
	SHOUTERR_BUSY		= -10
)
//line /home/ubuntu/workspace/cgotest.go:53

//line /home/ubuntu/workspace/cgotest.go:52
const (
	FORMAT_OGG	= 0
	FORMAT_MP3	= 1
	FORMAT_WEBM	= 2
)
//line /home/ubuntu/workspace/cgotest.go:59

//line /home/ubuntu/workspace/cgotest.go:58
const (
	PROTOCOL_HTTP		= 0
	PROTOCOL_XAUDIOCAST	= 1
	PROTOCOL_ICY		= 2
)
//line /home/ubuntu/workspace/cgotest.go:65

//line /home/ubuntu/workspace/cgotest.go:64
type ShoutError struct {
	Message	string
	Code	int
}
//line /home/ubuntu/workspace/cgotest.go:70

//line /home/ubuntu/workspace/cgotest.go:69
func (e ShoutError) Error() string {
	return fmt.Sprintf("%s (%d)", e.Message, e.Code)
}
//line /home/ubuntu/workspace/cgotest.go:74

//line /home/ubuntu/workspace/cgotest.go:73
type Shout struct {
						Host		string
						Port		uint
						User		string
						Password	string
						Mount		string
						Format		int
						Protocol	int
//line /home/ubuntu/workspace/cgotest.go:84

//line /home/ubuntu/workspace/cgotest.go:83
	struc		*_Ctype_struct_shout
						metadata	*_Ctype_struct_shout_metadata_t
//line /home/ubuntu/workspace/cgotest.go:87

//line /home/ubuntu/workspace/cgotest.go:86
	stream	chan []byte
}
//line /home/ubuntu/workspace/cgotest.go:90

//line /home/ubuntu/workspace/cgotest.go:89
func init() {
	_Cfunc_shout_init()
}
//line /home/ubuntu/workspace/cgotest.go:94

//line /home/ubuntu/workspace/cgotest.go:93
func Shutdown() {
	_Cfunc_shout_shutdown()
}
//line /home/ubuntu/workspace/cgotest.go:98

//line /home/ubuntu/workspace/cgotest.go:97
func Free(s *Shout) {
	_Cfunc_shout_free(s.struc)
}
//line /home/ubuntu/workspace/cgotest.go:102

//line /home/ubuntu/workspace/cgotest.go:101
func (s *Shout) lazyInit() {
	if s.struc != nil {
		return
	}
//line /home/ubuntu/workspace/cgotest.go:107

//line /home/ubuntu/workspace/cgotest.go:106
	s.struc = _Cfunc_shout_new()
						s.updateParameters()
//line /home/ubuntu/workspace/cgotest.go:110

//line /home/ubuntu/workspace/cgotest.go:109
	s.stream = make(chan []byte)
}
//line /home/ubuntu/workspace/cgotest.go:113

//line /home/ubuntu/workspace/cgotest.go:112
func (s *Shout) updateParameters() {
//line /home/ubuntu/workspace/cgotest.go:115

//line /home/ubuntu/workspace/cgotest.go:114
	p := _Cfunc_CString(s.Host)
						_Cfunc_shout_set_host(s.struc, p)
						_Cfunc_free(unsafe.Pointer(p))
//line /home/ubuntu/workspace/cgotest.go:120

//line /home/ubuntu/workspace/cgotest.go:119
	_Cfunc_shout_set_port(s.struc, _Ctype_ushort(s.Port))
//line /home/ubuntu/workspace/cgotest.go:123

//line /home/ubuntu/workspace/cgotest.go:122
	p = _Cfunc_CString(s.User)
						_Cfunc_shout_set_user(s.struc, p)
						_Cfunc_free(unsafe.Pointer(p))
//line /home/ubuntu/workspace/cgotest.go:128

//line /home/ubuntu/workspace/cgotest.go:127
	p = _Cfunc_CString(s.Password)
						_Cfunc_shout_set_password(s.struc, p)
						_Cfunc_free(unsafe.Pointer(p))
//line /home/ubuntu/workspace/cgotest.go:133

//line /home/ubuntu/workspace/cgotest.go:132
	p = _Cfunc_CString(s.Mount)
						_Cfunc_shout_set_mount(s.struc, p)
						_Cfunc_free(unsafe.Pointer(p))
//line /home/ubuntu/workspace/cgotest.go:138

//line /home/ubuntu/workspace/cgotest.go:137
	_Cfunc_shout_set_format(s.struc, _Ctype_uint(s.Format))
//line /home/ubuntu/workspace/cgotest.go:141

//line /home/ubuntu/workspace/cgotest.go:140
	_Cfunc_shout_set_protocol(s.struc, _Ctype_uint(s.Protocol))
}
//line /home/ubuntu/workspace/cgotest.go:144

//line /home/ubuntu/workspace/cgotest.go:143
func (s *Shout) GetError() string {
	s.lazyInit()
	err := _Cfunc_shout_get_error(s.struc)
	return _Cfunc_GoString(err)
}
//line /home/ubuntu/workspace/cgotest.go:150

//line /home/ubuntu/workspace/cgotest.go:149
func (s *Shout) Open() (chan<- []byte, error) {
						s.lazyInit()
//line /home/ubuntu/workspace/cgotest.go:153

//line /home/ubuntu/workspace/cgotest.go:152
	errcode := int(_Cfunc_shout_open(s.struc))
						if errcode != _Cconst_SHOUTERR_SUCCESS {
		return nil, ShoutError{
			Code:		errcode,
			Message:	s.GetError(),
		}
	}
//line /home/ubuntu/workspace/cgotest.go:161

//line /home/ubuntu/workspace/cgotest.go:160
	go s.handleStream()
//line /home/ubuntu/workspace/cgotest.go:163

//line /home/ubuntu/workspace/cgotest.go:162
	return s.stream, nil
}
//line /home/ubuntu/workspace/cgotest.go:166

//line /home/ubuntu/workspace/cgotest.go:165
func (s *Shout) Close() error {
	errcode := int(_Cfunc_shout_close(s.struc))
	if errcode != _Cconst_SHOUTERR_SUCCESS {
		return ShoutError{
			Code:		errcode,
			Message:	s.GetError(),
		}
	}
//line /home/ubuntu/workspace/cgotest.go:175

//line /home/ubuntu/workspace/cgotest.go:174
	return nil
}
//line /home/ubuntu/workspace/cgotest.go:178

//line /home/ubuntu/workspace/cgotest.go:177
func (s *Shout) send(buffer []byte) error {
						ptr := (*_Ctype_unsignedchar)(&buffer[0])
						_Cfunc_shout_send(s.struc, ptr, _Ctype_size_t(len(buffer)))
//line /home/ubuntu/workspace/cgotest.go:182

//line /home/ubuntu/workspace/cgotest.go:181
	errno := int(_Cfunc_shout_get_errno(s.struc))
						if errno != _Cconst_SHOUTERR_SUCCESS {
		fmt.Println("something went wrong: %d", errno)
	}
//line /home/ubuntu/workspace/cgotest.go:187

//line /home/ubuntu/workspace/cgotest.go:186
	_Cfunc_shout_sync(s.struc)
						return nil
}
//line /home/ubuntu/workspace/cgotest.go:191

//line /home/ubuntu/workspace/cgotest.go:190
func (s *Shout) handleStream() {
	for buf := range s.stream {
		s.send(buf)
	}
}
//line /home/ubuntu/workspace/cgotest.go:197

//line /home/ubuntu/workspace/cgotest.go:196
func (s *Shout) UpdateMetadata(mname string, mvalue string) {
	md := _Cfunc_shout_metadata_new()
	ptr1 := _Cfunc_CString(mname)
	ptr2 := _Cfunc_CString(mvalue)
	_Cfunc_shout_metadata_add(md, ptr1, ptr2)
	_Cfunc_free(unsafe.Pointer(ptr1))
	_Cfunc_free(unsafe.Pointer(ptr2))
	_Cfunc_shout_set_metadata(s.struc, md)
	_Cfunc_shout_metadata_free(md)
}
//line /home/ubuntu/workspace/cgotest.go:208

//line /home/ubuntu/workspace/cgotest.go:207
func main() {
	s := Shout{}
	fmt.Printf("%s", s)
}
