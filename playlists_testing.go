package main

import (
    "fmt"
    "./lib/playlist"
)

func main() {
    jsd, err := playlist.GetSongsAsJSON()
    if err != nil {
        fmt.Println(err)
        return
    }
    
    fmt.Println(jsd)
}