create table genres (
id serial not null primary key,
name varchar(100) not null
);

create table cities (
id serial not null primary key,
designator varchar(5) not null unique
);

create table locales (
id serial not null primary key,
city_designator varchar(5) not null,
foreign key (city_designator) references cities(designator)
);

create table artists (
id serial not null primary key,
name varchar(100) not null unique,
tags varchar(1000),
genre_id integer not null,
foreign key (genre_id) references genres(id)
);

create table songs (
sid varchar(32) not null primary key,
name varchar(100) not null,
tags varchar(1000),
artist_id integer not null,
genre_id integer not null,
foreign key (artist_id) references artists(id),
foreign key (genre_id) references genres(id)
);

create table listens (
song_id varchar(32) not null,
uid integer not null,
foreign key (song_id) references songs(sid)
);