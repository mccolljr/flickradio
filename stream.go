package main // will be stream after testing

import (
    _ "bytes"
    "os"
    "io"
    "log"
    "encoding/base64"
    "net/http"
)

var _ = base64.StdEncoding

const (
    FromStart = 0
    FromHere  = 1
    FromEnd   = 2
    ChunkSize = 96*1024
)

func chunk(src io.ReadSeeker, dest io.Writer, start int64) {
    //b64 := base64.NewEncoder(base64.StdEncoding, dest)
    _, err := src.Seek(start, FromStart)
    if err != nil {
        panic(err)
    }
    
    // TODO: handle when the new offset isn't the same as the intended offset
    buff := make([]byte, ChunkSize)
    n, err := src.Read(buff)
    if err != io.EOF && err != nil {
        panic(err)
    }
    
    dest.Write(buff[:n])
    //b64.Write(buff[:n])
}

func chunk2(src io.Reader, dest io.Writer, start int64) {
    buff := make([]byte, 16*1024)
    _ = start
    for {
        n, err := src.Read(buff);
        if err != io.EOF && err != nil {
            panic(err)
        }
        if n == 0 {
            break
        }
        dest.Write(buff);
    }
}

func main() {
    pos := int64(0);
    file, err := os.Open("x.mp3")
    if err != nil {
        log.Fatal(err)
    }
    
    http.HandleFunc("/player", func(w http.ResponseWriter, r *http.Request){
        bu := make([]byte, 16*1024)
        f, err := os.Open("SV.html")
        if err != nil {
            http.Error(w, err.Error(), 500)
        }
        
        for {
            n, err := f.Read(bu)
            if err != io.EOF && err != nil {
                http.Error(w, err.Error(), 500)
                break
            }
            if n == 0 {
                break
            }
            w.Write(bu[:n]);
        }
    })
    
    http.HandleFunc("/stream", func(w http.ResponseWriter, r *http.Request){
        chunk2(file, w, pos*ChunkSize)
        //log.Println("Serving chunk", pos)
        //pos = pos + 1;
    })
    
    log.Fatal(http.ListenAndServe(":8080", nil))
}