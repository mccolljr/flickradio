package playlist

import (
    "../dbi"
    "encoding/json"
    _ "log"
    _ "fmt"
)

// This method returns all the songs in the database as JSON string...

func GetSongsAsJSON() (string, error) {
    sungs, err := dbi.GetSongs()
    if err != nil {
        return "", err
    }
    
    data, err := json.Marshal(sungs)
    if err != nil {
        return "", err
    }
    
    return string(data), nil
}