package render

import (
    "log"
    "html/template"
    "net/http"
)

var defaultLayoutFile string = "views/layout.html"

type PageLayout struct {
    Content interface{}
}

func PageWithLayout(w http.ResponseWriter, layoutFileName string, contentFileName string, contentContext interface{}) error {
    log.Println("rendering", contentFileName)
    page, err := template.ParseFiles(layoutFileName, contentFileName);
    if err != nil {
        return err
    }
    page.Execute(w, PageLayout{contentContext})
    return nil
}

func Page(w http.ResponseWriter, fileName string, context interface{}) error {
    return PageWithLayout(w, defaultLayoutFile, fileName, context)
}

func Error(w http.ResponseWriter, message string, code int){
    e := struct {
        Message string
        Code int
    }{message, code}
    
    err := PageWithLayout(w, defaultLayoutFile, "views/error.html", e)
    
    if err != nil {
        http.Error(w, message, code)
    }
}