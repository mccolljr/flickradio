package dbi

import (
    "strings"
    "bytes"
    _ "log"
    "fmt"
    _ "os"
    "database/sql"
    _ "github.com/lib/pq"
    "github.com/satori/go.uuid"
)

const driver = "postgres"
const dburl  = "user=ubuntu password=mokitamu database=media sslmode=disable"

var db *sql.DB

//var statements map[string]*sql.Stmt

func init(){
    // if anything in this method fails, panic
    // the application should under no circumstances
    // fail to connect to the database - 
    // this is a breaking problem,
    // so it SHOULD break if this should occur
    var err error
    db, err = sql.Open(driver, dburl)
    if err != nil {
        panic(err)
    }
}

// Declare types

type Song struct {
    SID string
    Name string
    GenreId int
    ArtistId int
    Tags string
}

type Genres map[int]string

// Methods for Song type

// PUBLIC

func NewSong() *Song{
    s := &Song{SID: uuid.NewV4().String()}
    
    return s;
}

func (s *Song) Save() error {
    var sad string
    err := db.QueryRow("select sid from songs where sid=$1", s.SID).Scan(&sad)
    switch err {
    case sql.ErrNoRows:
        _, err = db.Exec("insert into songs values ($1, $2, $3, $4, $5)",
                            s.SID, s.Name, s.Tags, s.ArtistId, s.GenreId)
    case nil:
        var queryWriter bytes.Buffer
        ct := 0;
        sets := make([]string, 0)
        args := make([]interface{}, 0);
        if s.Name != "" {
            ct++
            sets = append(sets, fmt.Sprintf(" name=$%v", ct))
            args = append(args, s.Name)
        }
        if s.Tags != "" {
            ct++
            sets = append(sets, fmt.Sprintf(" tags=$%v", ct))
            args = append(args, s.Tags)
        }
        if s.ArtistId > 0 {
            ct++
            sets = append(sets, fmt.Sprintf(" artist_id=$%v", ct))
            args = append(args, s.ArtistId)
        }
        if s.GenreId > 0 {
            ct++
            sets = append(sets, fmt.Sprintf(" genre_id=$%v", ct))
            args = append(args, s.GenreId)
        }
        
        queryWriter.WriteString("update songs set")
        queryWriter.WriteString(strings.Join(sets, ","))
        ct++
        args = append(args, s.SID)
        queryWriter.WriteString(fmt.Sprintf(" where sid=$%v", ct))
        
        _, err = db.Exec(queryWriter.String(), args...)
        
    default:
        return err
    }
    
    if err != nil {
        return err
    }
    
    return nil;
}

// Data retrieval methods
// methods meant only to retrieve data from the database
// and which do not require an interface for updating or inserting

func GetGenres() (Genres, error) {
    var genreId int
    var genreName string
    
    results := make(Genres)
    
    rows, err := db.Query("select name, id from genres;");
    
    if err != nil {
        return nil, err
    }
    
    for rows.Next() {
        err = rows.Scan(&genreName, &genreId)
        if err != nil {
            return nil, err
        }
        results[genreId] = genreName
    }
    
    err = rows.Err()
    
    if err != nil {
        return nil, err
    }
    
    return results, nil
}

func GetSongs() ([]*Song, error){
    
    results := make([]*Song, 0)
    
    rows, err := db.Query("select * from songs")
    
    if err != nil {
        return nil, err
    }
    
    for rows.Next() {
        song := &Song{}
        err = rows.Scan(&song.SID, &song.Name, &song.Tags, &song.ArtistId, &song.GenreId)
        if err != nil {
            return nil, err
        }
        
        results = append(results, song)
    }
    
    return results, nil
}